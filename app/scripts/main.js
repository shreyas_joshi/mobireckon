$(document)
    .ready(function () {
        $(function () {
            $('a[href*="#"]:not([href="#"])')
                .click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length
                            ? target
                            : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html, body').animate({
                                scrollTop: target
                                    .offset()
                                    .top - 49
                            }, 700);
                            return false;
                        }
                    }
                });
        });

        var mobileMenuButton = document.getElementById('mobile-menu-btn');
        var mobileMenu = document.getElementById('mobile-menu');
        var mobileMenuLinks = document.querySelectorAll('section#mobile-menu a.nav-item');
        console.log(mobileMenuLinks);
        console.log(mobileMenuButton);
        console.log(mobileMenu);
        
        mobileMenuButton.addEventListener('click', function(event){
            event.stopPropagation();
            if (mobileMenu.classList.contains('menu-open')){
                mobileMenu.classList.remove('menu-open');
                mobileMenuButton.innerHTML = null;
                mobileMenuButton.innerHTML = '<i class="fas fa-bars"></i>'
            }
            else {
                mobileMenu.classList.add('menu-open');
                mobileMenuButton.innerHTML = null;
                mobileMenuButton.innerHTML = '<i class="fas fa-times"></i>'
            }
        }, false)

        for (var index = 0 ; index < mobileMenuLinks.length ; ++index) {
            mobileMenuLinks[index].addEventListener('click', function() {
                if (mobileMenu.classList.contains('menu-open')){
                    mobileMenu.classList.remove('menu-open');
                    mobileMenuButton.innerHTML = null;
                    mobileMenuButton.innerHTML = '<i class="fas fa-bars"></i>'
                }
            });
        }

        document.addEventListener('click', function(){
            if (mobileMenu.classList.contains('menu-open')){
                mobileMenu.classList.remove('menu-open');
                mobileMenuButton.innerHTML = null;
                mobileMenuButton.innerHTML = '<i class="fas fa-bars"></i>'
            }
        });
    });